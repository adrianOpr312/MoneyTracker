# MoneyTracker
#
### A money tracking application with a couple of functionalities:
- Can make an account
- Can add money to balance
- Can remove money from balance
- Can change the default currency (without converting)
- Can change the default currency (with conversion)
- Can reserve a certain sum for a specific reason
- Can complete the reservation (will archive reservation as completed)
- Can remove reservation (returns the money to the account)